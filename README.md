# Municipality Pention Software

- Pention Fund Entry and Report for Puducherry Municipality

### Front-End

- `HTML5`
- `Bulma-CSS Framework`
- `Java script`

### Back-End

- `Django Web Framework`

### Database

- `MariaDB`

## LICENSE

In development
