from datetime import datetime
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.utils import datetime_safe


class TypesOfPension(models.Model):
    type = models.CharField(max_length=10, primary_key=True)
    full_form = models.CharField(max_length=256, null=True, blank=True)
    details = models.TextField(blank=True, null=True)

    def __str__(self) -> str:
        return f'{self.type} | {self.full_form}'
    


class Employee(models.Model):
    BANK_CHOICE = (
        ('IOB_M','Indian Overseas Bank - Town Branch'),
        ('IOB_O', 'Indian Overseas Bank - Other Branch'),
        ('SBI_M','State Bank of India - Main Branch'),
        ('SBI_O', 'State Bank of India - Other Branch'),
    )
    AGE_CHOICES=(
        ('below_80','Below 80 years'),
        ('80','80 years'),
        ('85','85 years'),
        ('90','90 years'),
    )

    bank = models.CharField(max_length=7, choices=BANK_CHOICE, verbose_name=_('Select Bank Branch'))
    pk_ppo = models.CharField(max_length=256, primary_key=True)
    monthly_date = models.CharField(max_length=7, verbose_name=_('Date'))
    ppo = models.CharField(max_length=200,null=True,blank=True)
    acc = models.PositiveIntegerField(null=True, blank=True)
    name = models.CharField(max_length=200, null=True, blank=True)
    age = models.CharField(max_length=200,choices = AGE_CHOICES,blank=True)
    designation = models.CharField(max_length=256, null=True, blank=True)
    types = models.ForeignKey(TypesOfPension, on_delete=models.RESTRICT, null=True)
    basic = models.PositiveIntegerField(null=True, blank=True)
    additional = models.PositiveIntegerField(null=True, blank=True)
    total1 = models.PositiveIntegerField(null=True, blank=True)
    comuted = models.PositiveIntegerField(null=True, blank=True, default=0)
    residuary = models.PositiveIntegerField(null=True, blank=True)
    dr = models.PositiveIntegerField(null=True, blank=True)
    fma = models.PositiveIntegerField(null=True, blank=True)
    emolume = models.PositiveIntegerField(null=True, blank=True)
    amount = models.PositiveIntegerField(null=True, blank=True)
    less = models.PositiveIntegerField(null=True, blank=True)
    Total2 = models.PositiveIntegerField(null=True, blank=True)
    remarks = models.CharField(max_length=200,null=True, blank=True)
    remarks_text = models.TextField(null=True, blank=True)
    remarks_date = models.CharField(null=True, max_length=10, verbose_name=_('Date for Remarks'))
    issued_at = models.DateField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True)
    updated_at = models.DateTimeField(auto_now=True)

    def save(self, *args, **kwargs):
        if self.remarks_text:
            formatted_date = self.remarks_date.strftime('%d-%m-%Y') if self.remarks_date is not None else ''
            self.remarks = self.remarks_text + (f" ({formatted_date})" if formatted_date else '')
        super().save(*args, **kwargs)


    def __str__(self):
        return f'{self.ppo} | {self.monthly_date}'

    @staticmethod
    def calculate_group_totals(group, prev_totals=None):
        fields = ['basic', 'additional', 'total1', 'comuted', 'residuary', 'dr', 'fma', 'emolume', 'amount', 'less', 'Total2']
        group_totals = {field: sum(getattr(item, field, 0) or 0 for item in group) for field in fields}

        if prev_totals is not None:
            print(type(prev_totals))
            group_totals = {j: group_totals[j] + prev_totals.get(j, 0) for j in group_totals}
        return group_totals

    





