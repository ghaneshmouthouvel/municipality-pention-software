from django.apps import AppConfig


class PensionCandidatesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pension_candidates'
