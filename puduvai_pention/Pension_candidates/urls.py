from django.urls import path, re_path
from .import views
from .views import export,index,dashboard,dashboard_month
urlpatterns = [
    path('create/', views.create, name='create'),
    path('bankoption/', views.bankoption, name="bankoption"),
    path('bankoption/import/<str:bank>/', views.import_data, name="import"),
    path('dashboard/', dashboard, name='dashboard'),
    path('dashboard/<str:month>', dashboard_month, name='dashboard_month'),
    path('<str:month>/<str:bank>/statement/', views.statement, name='statement'),
    path('<str:month>/<str:bank>/Edit', index, name='edit'),
    path('<str:month>/<str:bank>/export/', export, name="export"),
    path('<str:month>/<str:bank>/statement/export/', views.export_statement, name="export_statement"),
    path('genform/', views.generateDataNextMonth, name='genform'),
    re_path(r'^update/(?P<pk_ppo>.+)/$', views.update, name='update'),
    re_path(r'^delete/(?P<pk_ppo>.+)/$', views.delete, name='delete'),
    re_path(r'^search/(?P<pk_ppo>.+)/$', views.search, name='search'),
    path('notification/', views.notification, name='notification'),


    ]
