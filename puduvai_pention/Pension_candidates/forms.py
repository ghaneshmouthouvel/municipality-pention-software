from django import forms
from .models import Employee,TypesOfPension
from datetime import date


class EmployeeForm(forms.ModelForm):
    bank = forms.ChoiceField(
    choices=[('', '---------')] + list(Employee.BANK_CHOICE),
    widget=forms.Select(attrs={'class': 'form-control'}),
    label = 'Select bank Branch'
    )
    types = forms.ModelChoiceField(
        queryset=TypesOfPension.objects.all(),
        widget=forms.Select(attrs={'class': 'form-control'}),
        label = 'Types Of pension'
    )
    basic = forms.IntegerField(label='Revised Basic Pension')
    # additional = forms.IntegerField(label='Revised Additional Pension')
    comuted = forms.IntegerField(label='Less Commuted Value')
    issued_at = forms.DateField(widget=forms.SelectDateWidget(attrs={'class': 'form-control'}), required=False)
    age = forms.ChoiceField(
    choices=[('', '---------')]+list(Employee.AGE_CHOICES),
    widget=forms.Select(attrs={'class': 'form-control'}),
    label = 'Choose Age'
    )
    remarks_date = forms.DateField(required=False,widget=forms.DateInput(attrs={'type': 'date'}))
    
    
    def clean(self):
        cleaned_data = super().clean()
        age = cleaned_data.get('age')
        basic = cleaned_data.get('basic')
        additional = cleaned_data.get('additional')
        comuted = cleaned_data.get('comuted')
        issued_at = cleaned_data.get('issued_at')
        if issued_at:
         if issued_at > date.today():
            self.add_error('issued_at', 'Issued at date must be in the past')
        if age == 'below_80' and basic and not additional:
            additional = basic * 0 
            cleaned_data['additional'] = additional
        elif age == '80' and basic and not additional:
            additional = basic * 0.20
            cleaned_data['additional'] = additional
        elif age == '85' and basic and not additional:
            additional = basic * 0.30
            cleaned_data['additional'] = additional 
        elif age == '90' and basic and not additional:
            additional = basic * 0.40
            cleaned_data['additional'] = additional
        if basic and additional:
            cleaned_data['total1'] = basic + additional
            cleaned_data['dr'] = cleaned_data['total1'] * 0.38
        if cleaned_data.get('total1') and comuted:
            cleaned_data['residuary'] = cleaned_data['total1'] - comuted
        if cleaned_data.get('residuary') and cleaned_data.get('dr') and cleaned_data.get('fma'):
            cleaned_data['emolume'] = cleaned_data['residuary'] + cleaned_data['dr'] + cleaned_data['fma']
        if cleaned_data.get('emolume') and cleaned_data.get('amount') and cleaned_data.get('less'):
            cleaned_data['Total2'] = cleaned_data['emolume'] + cleaned_data['amount'] - cleaned_data['less']
        return cleaned_data
    
    class Meta:
        model = Employee
        fields = (
            'bank', 'monthly_date', 'ppo', 'acc', 'name', 'designation',
            'types', 'basic', 'additional', 'total1',
            'comuted', 'residuary', 'dr', 'fma', 'emolume',
            'amount', 'less', 'Total2', 'remarks_text','issued_at',
        )
        
        widgets = {
             'ppo': forms.TextInput(attrs={'class': 'form-control'}),
             'monthly_date': forms.TextInput(attrs={'type':'month'}),
             'residuary': forms.NumberInput(attrs={'disabled': 'disabled', 'placeholder': 'Total Pension + Less Commuted Value'}),
             'total1': forms.TextInput(attrs={'disabled': 'disabled', 'placeholder': 'Basic Pension + Additional Pension'}),
             'additional': forms.TextInput(attrs={'disabled': 'disabled', 'placeholder': 'Basic Pension * Selected Age'}),
             'dr': forms.TextInput(attrs={'disabled': 'disabled', 'placeholder': 'Total Pension * 0.38'}),
             'emolume': forms.TextInput(attrs={'disabled': 'disabled', 'placeholder': 'Residuary+DR+F.M.A.'}),
             'Total2': forms.TextInput(attrs={'disabled': 'disabled', 'placeholder': 'Final amount'}),

}

        labels = {
            'ppo': 'PPO No',
            'acc': 'Account Number',
            'monthly_date': 'Date',
            'name': 'Name',
            'designation': 'Designation',
            'types': 'Types of pension',
            'basic': 'Revised Basic Pension',
            'additional': 'Revised Additional Pension on 80 years above',
            'total1': 'Total Pension',
            'comuted': 'Less Commuted Value',
            'residuary': 'Residuary Pension',
            'dr': 'DR 38%',
            'fma': 'F.M.A',
            'emolume': 'Total Emolume nts(7th NPC)',
            'amount': 'Add Pension Amount',
            'less': 'Less Excess Amount',
            'Total2': 'Total',
            'remarks_text': 'Remarks',
            'remarks_date': 'Remarks with Date'
        }
