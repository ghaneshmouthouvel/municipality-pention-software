from django.shortcuts import render, redirect, get_object_or_404
from django.urls import reverse
from django.db import IntegrityError
from django.contrib.auth.decorators import login_required
from .forms import EmployeeForm
from .models import Employee, TypesOfPension
from openpyxl import Workbook
from openpyxl import load_workbook
from django.http import Http404
from datetime import date,datetime, timedelta
import re
from django.db.models import Sum,Q,F
from django.contrib import messages
from itertools import zip_longest
# Create your views here.

@login_required(login_url='')
def dashboard(request):
    if request.method == 'GET':
        month = request.GET.get('selected_month', date.today().strftime("%Y-%m"))
        return redirect('dashboard_month', month=month)
    
    return render(request, 'dashboard.html', {'selected_month' : month})

def dashboard_month(request, month):
    iob_m_count = Employee.objects.filter(bank='IOB_M', monthly_date=month).count()
    iob_o_count = Employee.objects.filter(bank='IOB_O', monthly_date=month).count()
    sbi_m_count = Employee.objects.filter(bank='SBI_M', monthly_date=month).count()
    sbi_o_count = Employee.objects.filter(bank='SBI_O', monthly_date=month).count()
    counts = {'iob_m_count': iob_m_count, 'iob_o_count': iob_o_count, 'sbi_m_count':sbi_m_count, 'sbi_o_count':sbi_o_count}
    return render(request, 'dashboard.html', {'selected_month': month, 'counts': counts})

def index(request, month, bank):
    bank_name = dict(Employee.BANK_CHOICE)[bank]
    template_name = 'pension_bank.html'
    obj = Employee.objects.filter(bank=bank, monthly_date = month)
    title = f"PAYMENT OF PENSION FOR THE MONTH OF {datetime.strptime(month, '%Y-%m').strftime('%B %Y').upper()} THROUGH {bank_name.upper()}"
    context = {'data': obj, 'title': title, 'export_url': reverse('export', kwargs={'bank': bank, 'month': month}) }
    for emp in obj:
        pk_ppo = f"{emp.ppo}/{emp.monthly_date}"
        emp.delete_url = reverse('delete', kwargs={'pk_ppo':pk_ppo})
        emp.update_url = reverse('update', kwargs={'pk_ppo': pk_ppo})
    return render(request, template_name, context)

def chunk_list(lst, chunk_size):
    return [lst[i:i+chunk_size] for i in range(0, len(lst), chunk_size)]


def statement(request, month, bank):
    obj = Employee.objects.filter(bank=bank, monthly_date=month).order_by('ppo')
    grouped_obj = chunk_list(list(obj), 10)
    group_totals = []
    total_sum = None
    for group in grouped_obj:
        group_total = Employee.calculate_group_totals(group, prev_totals=total_sum)
        group_totals.append(group_total)
        total_sum = group_total
    title = f"PENSION STATEMENT FOR THE MONTH OF {datetime.strptime(month, '%Y-%m').strftime('%B %Y').upper()} THROUGH {dict(Employee.BANK_CHOICE)[bank].upper()}"
    export1_url = reverse('export_statement', kwargs={'bank': bank, 'month': month})
    return render(request, 'statement.html', {'grouped_data': zip(grouped_obj, group_totals), 'title': title, 'export1_url': export1_url})

def search(request, pk_ppo):
    if pk_ppo:
        try:
            ppo, monthly_date = pk_ppo.split('/')
            selected_month, selected_year = monthly_date.split('-')
            pensioner = Employee.objects.filter(monthly_date=monthly_date, ppo=ppo).first()
            if pensioner:
                bank_name = pensioner.bank
                bank_full_name_map = dict(Employee.BANK_CHOICE)
                full_name = bank_full_name_map.get(bank_name, "Unknown")
                pensioner.delete_url = reverse('delete', kwargs={'pk_ppo':pk_ppo})
                pensioner.update_url = reverse('update', kwargs={'pk_ppo': pk_ppo})
                return render(request, 'search_result.html', {'pensioner': pensioner, 'bank_name': full_name})
            else:
                raise IntegrityError
        except (ValueError, Employee.DoesNotExist, IntegrityError):
            error = "Pensioner does not exist"
            return render(request, 'search_result.html', {'error': error})
    else:
        return render(request, 'search_result.html')


def create(request):
    if request.method == "POST":
        form = EmployeeForm(request.POST)
        form.is_valid()
        print(form.errors)
        if form.is_valid():
            employee_data: dict = form.cleaned_data
            employee_data['pk_ppo'] = f"{employee_data['ppo']}/{employee_data['monthly_date']}"
            total1 = employee_data['basic'] + employee_data['additional']
            employee_data['total1'] = total1
            residuary = total1 - employee_data['comuted']
            employee_data['residuary'] = residuary
            dr = total1 * 0.38  
            employee_data['dr'] = dr
            emolume = employee_data['residuary'] + employee_data['dr'] + employee_data['fma']
            employee_data['emolume'] = emolume
            if employee_data.get('amount'):
                amount = employee_data.get('amount')
            else:
                amount = 0
            if employee_data.get('less'):
                less = employee_data.get('less')
            else:
                less = 0
            total2 = emolume + amount - less
            employee_data['Total2'] = total2
            issued_at = date.today()
            employee_data['issued_at'] = issued_at
            try:
                employee = Employee.objects.create(**employee_data)
            except IntegrityError:
                return render(request, 'create.html', {'form':form, 'error':'Candidates already exist'})
            url = reverse('edit', kwargs={'month': datetime.strptime(employee.monthly_date, "%Y-%m").strftime("%Y-%m"), 'bank': employee.bank})
            return redirect(url)
        return redirect('create')    
    else:
        form = EmployeeForm()        
    return render(request, 'create.html', {'form': form})

def update(request, pk_ppo):
    employee = get_object_or_404(Employee, pk_ppo=pk_ppo)
    if request.method == "POST":
        form = EmployeeForm(request.POST)
        if form.is_valid():
            employee_data: dict = form.cleaned_data
            total1 = employee_data['basic'] + employee_data['additional']
            employee_data['total1'] = total1
            residuary = total1 - employee_data['comuted']
            employee_data['residuary'] = residuary
            dr = total1 * 0.38  
            employee_data['dr'] = dr
            emolume = employee_data['residuary'] + employee_data['dr'] + employee_data['fma']
            employee_data['emolume'] = emolume
            if employee_data.get('amount'):
                amount = employee_data.get('amount')
            else:
                amount = 0
            if employee_data.get('less'):
                less = employee_data.get('less')
            else:
                less = 0
            total2 = emolume + amount - less
            employee_data['Total2'] = total2
            issued_at = date.today()
            employee_data['issued_at'] = issued_at
            # process data
            if employee_data['monthly_date'] == employee.monthly_date:
                # update existing record
                for key, value in employee_data.items():
                    setattr(employee, key, value)
                employee.save()
            else:
                # create new record
                employee_data['pk_ppo'] = f"{employee_data['ppo']}/{employee_data['monthly_date']}"
                Employee.objects.create(**employee_data)
            url = reverse('edit', kwargs={'month': datetime.strptime(employee.monthly_date, "%Y-%m").strftime("%Y-%m"), 'bank': employee.bank})
            return redirect(url)
    else:
        form = EmployeeForm(instance=employee,initial={'age': employee.age, 'remarks_date': employee.remarks_date})
    return render(request, 'update.html', {'form': form})


def delete(request,pk_ppo):
    employee = get_object_or_404(Employee, pk_ppo=pk_ppo)
    employee.delete()
    return redirect('edit', month=employee.monthly_date, bank=employee.bank)


def bankoption(request):
    return render(request,'bank.html')


from django.http import HttpResponse
from openpyxl.utils import get_column_letter
from openpyxl.styles import Font, Alignment


def export(request, bank, month):
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = f'attachment; filename="Editing_{month}_{bank}.xlsx"'
    data = Employee.objects.filter(bank=bank, monthly_date=month).order_by('ppo')

    wb = Workbook()
    ws = wb.active

    widths = [5.11, 6.78, 7.67, 33, 15.78, 8.89, 10, 7, 10, 10, 10, 16, 10, 20, 20, 20, 20, 20, 15]
    for i, width in enumerate(widths):
     col_letter = get_column_letter(i+1)
     ws.column_dimensions[col_letter].width = width
 

    # Add header row
    ws.append(['Sl No', 'PPO No','A/c No.','Choose Age','Name','Designation',
               'Types of Pension','Revised Basic pension','Revised Additional Pension of 80 years above',
               'Total Pension','Less Commuted Value','Residuary Pension','DR 38%','F.M.A',
               'Total Emoulme nts(7th CPC)','Add Pension Amount','Less Excess Amount',
               'Total(7th CPC)','Remarks','Remarks with Text','Remarks with Date','Monthly Date'])

    # Add data rows
    for i, user in enumerate(data):
     ws.append([i+1, user.ppo, user.acc,user.age ,user.name,
                user.designation, user.types.type, user.basic,
                  user.additional, user.total1,user.comuted,
                    user.residuary, user.dr, user.fma, user.emolume,
                    user.amount, user.less,user.Total2,user.remarks,user.remarks_text,user.remarks_date,user.monthly_date])


    wb.save(response)
    return response

def export_statement(request, month, bank):
    response = HttpResponse(content_type='application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')
    response['Content-Disposition'] = f'attachment; filename="statement_{month}_{bank}.xlsx"'
    obj = Employee.objects.filter(bank=bank, monthly_date=month).order_by('ppo')
    grouped_obj = chunk_list(list(obj), 10)
    group_totals = []
    total_sum = None
    for group in grouped_obj:
        group_total = Employee.calculate_group_totals(group, prev_totals=total_sum)
        group_totals.append(group_total)
        total_sum = group_total

    wb = Workbook()
    ws = wb.active

    widths = [5.11, 6.78, 7.67, 33, 15.78, 8.89, 10, 7, 10, 10, 10, 16, 10, 20, 20, 20, 20, 20]
    for i, width in enumerate(widths):
        col_letter = get_column_letter(i+1)
        ws.column_dimensions[col_letter].width = width
    
    month_name = datetime.strptime(month, '%Y-%m').strftime('%B %Y').upper()
    bank_name = dict(Employee.BANK_CHOICE)[bank].upper()

    title1 = "PONDICHERRY MUNICIPALITY"
    title2 = "ACCOUNT SECTION"
    title3 = "PUDUCHERRY"
    title4 = "*****"
    title5 = f"PAYMENT OF PENSION FOR THE MONTH OF {month_name} THROUGH {bank_name}"

    ws.merge_cells('A1:R1')  # Merge cells from A1 to R1
    ws['A1'] = title1  # Set the title in cell A1
    ws['A1'].font = Font(name='Arial', size=12, bold=True)
    ws['A1'].alignment = Alignment(horizontal='center')
    ws.merge_cells('A2:R2')
    ws['A2'] = title2  # Set the title in cell A1
    ws['A2'].font = Font(name='Arial', size=12, bold=True)
    ws['A2'].alignment = Alignment(horizontal='center')
    ws.merge_cells('A3:R3')
    ws['A3'] = title3  # Set the title in cell A1
    ws['A3'].font = Font(name='Arial', size=12, bold=True)
    ws['A3'].alignment = Alignment(horizontal='center')
    ws.merge_cells('A4:R4')
    ws['A4'] = title4  # Set the title in cell A1
    ws['A4'].font = Font(name='Arial', size=12, bold=True)
    ws['A4'].alignment = Alignment(horizontal='center')
    ws.merge_cells('A5:R5')
    ws['A5'] = title5  # Set the title in cell A1
    ws['A5'].font = Font(name='Arial', size=12, bold=True)
    ws['A5'].alignment = Alignment(horizontal='center')


    ws.append([])
    for col in ws.iter_cols(min_col=1, max_col=26, min_row=6, max_row=6):
     for cell in col:
        cell.value = None

    # Add header row
    ws.append(['Sl No', 'PPO No','A/c No.','Name','Designation',
               'Types of Pension','Revised Basic pension','Revised Additional Pension of 80 years above',
               'Total Pension','Less Commuted Value','Residuary Pension','DR 38%','F.M.A',
               'Total Emoulments(7th CPC)','Add Pension Amount','Less Excess Amount',
               'Total(7th CPC)','Remarks'])

    # Add data rows for each group
    for group_index, (group, group_total) in enumerate(zip(grouped_obj, group_totals)):
        for i, user in enumerate(group):
            ws.append([i+1 + 10 * group_index, user.ppo, user.acc, user.name,
                       user.designation, user.types.type, user.basic,
                       user.additional, user.total1,user.comuted,
                       user.residuary, user.dr, user.fma, user.emolume,
                       user.amount, user.less,user.Total2,user.remarks])
        # Add group totals row
        ws.append(['', '', '', '', '', 'C/O', group_total['basic'],
                   group_total['additional'], group_total['total1'],
                   group_total['comuted'], group_total['residuary'],
                   group_total['dr'], group_total['fma'], group_total['emolume'],
                   group_total['amount'], group_total['less'],
                   group_total['Total2']])
        ws.append(['', '', '', '', '', 'B/F', group_total['basic'],
                   group_total['additional'], group_total['total1'],
                   group_total['comuted'], group_total['residuary'],
                   group_total['dr'], group_total['fma'], group_total['emolume'],
                   group_total['amount'], group_total['less'],
                   group_total['Total2']])

    wb.save(response)
    return response





from django.shortcuts import render

from openpyxl import load_workbook
from .models import Employee


def import_data(request, bank):
    bank_name = dict(Employee.BANK_CHOICE)[bank]
    
    if request.method == 'POST':
        file = request.FILES['file']
        wb = load_workbook(file)
        ws = wb.active
        

        for row in ws.iter_rows(min_row=2):
            ppo = row[1].value
            monthly_date = date.today().strftime("%Y-%m")
            pk_ppo = f'{ppo}/{monthly_date}'
            remarks_date_str = row[20].value
            remarks_date = datetime.strptime(remarks_date_str, "%Y-%m-%d").date() if remarks_date_str else None

            employee_data = {
                'bank': bank,
                'pk_ppo': pk_ppo,
                'monthly_date': monthly_date,
                'ppo': ppo,
                'acc': row[2].value,
                'age': row[3].value,
                'name': row[4].value,
                'designation': row[5].value,
                'types' :TypesOfPension.objects.get(type=row[6].value),
                'basic': row[7].value,
                'additional': row[8].value,
                'total1': row[9].value,
                'comuted': row[10].value,
                'residuary': row[11].value,
                'dr': row[12].value,
                'fma': row[13].value,
                'emolume': row[14].value,
                'amount': row[15].value,
                'less': row[16].value,
                'Total2': row[17].value,
                'remarks': row[18].value,
                'remarks_text': row[19].value,
                'remarks_date': remarks_date,
                'issued_at': date.today()
            }
            
            employee, created = Employee.objects.update_or_create(
                pk_ppo=employee_data['pk_ppo'], defaults=employee_data)
            
            url = reverse('edit', kwargs={'month': monthly_date, 'bank': bank})
        return redirect(url)
    
    return render(request, 'import.html', {'bank_name': bank_name})



# Employee.objects.filter(bank='IOB_M').count()

from datetime import datetime, timedelta

def notification(request):
    remarks = ['Go to Abroad', 'Working', 'Already Working']
    employee_by_remarks = {}
    for remark in remarks:
        employees = Employee.objects.filter(Q(remarks__icontains=remark) & ~Q(fma=0))
        if employees.exists():
            employee_by_remarks[remark] = employees.values('pk_ppo', 'ppo', 'acc', 'name', 'fma', 'monthly_date', 'remarks')
        else:
            messages.warning(request, f"No employees found with remark: {remark}") 
    double_pension_employees = Employee.objects.filter(remarks__icontains='Double Pension').exclude(ppo=F('acc'))
    print(f"Number of double pension employees: {double_pension_employees.count()}")
    for employee in double_pension_employees:
     messages.warning(request, f"Employee with PPO {employee.ppo} has Double Pension, but ACC is in a different PPO. Please cut Medical Allowance.")
    employee_by_remarks['Double Pension'] = double_pension_employees.values('pk_ppo', 'ppo', 'acc', 'name', 'fma', 'monthly_date', 'remarks')
    for remark in ['CVP completed', 'Life Certificate not produced', 'Expired on']:
        employees = Employee.objects.filter(remarks__icontains=remark)
        if remark == 'CVP completed':
            employees = employees.filter(remarks_date__gte=datetime.now().date()-timedelta(days=7))
        elif remark == 'Expired on':
            employees = employees.filter(remarks_date__gte=datetime.now().date()-timedelta(days=7))
        if employees.exists():
            employee_by_remarks[remark] = employees.values('pk_ppo', 'ppo', 'acc', 'name', 'fma', 'monthly_date', 'remarks', 'remarks_date')
            messages.warning(request, f"Some employees have {remark} remark and the remarks date is less than or equal to a week from today's date")
    half_pension_employees = Employee.objects.filter(remarks__icontains='Half Pension')
    if half_pension_employees.exists():
     employee_by_remarks['Half Pension'] = half_pension_employees.values('pk_ppo', 'ppo', 'acc', 'name', 'fma', 'monthly_date', 'remarks')
    else:
     messages.warning(request, "No employees found with remark: Half Pension")
    sixthpay_pension_employees = Employee.objects.filter(remarks__icontains='6th Pay')
    if sixthpay_pension_employees.exists():
     employee_by_remarks['6th Pay'] = sixthpay_pension_employees.values('pk_ppo', 'ppo', 'acc', 'name', 'fma', 'monthly_date', 'remarks')
    else:
     messages.warning(request, "No employees found with remark: 6th Pay")
    fifthpay_pension_employees = Employee.objects.filter(remarks__icontains='5th Pay')
    if fifthpay_pension_employees.exists():
     employee_by_remarks['5th Pay'] = fifthpay_pension_employees.values('pk_ppo', 'ppo', 'acc', 'name', 'fma', 'monthly_date', 'remarks')
    else:
     messages.warning(request, "No employees found with remark: 5th Pay")
    widow_pension_employees = Employee.objects.filter(remarks__icontains='Widow')
    if widow_pension_employees.exists():
     employee_by_remarks['Widow'] = widow_pension_employees.values('pk_ppo', 'ppo', 'acc', 'name', 'fma', 'monthly_date', 'remarks')
    else:
     messages.warning(request, "No employees found with remark: Widow Pension")
    cvpnot_pension_employees = Employee.objects.filter(remarks__icontains='Cvp not issued')
    if cvpnot_pension_employees.exists():
     employee_by_remarks['Cvp not issued'] = cvpnot_pension_employees.values('pk_ppo', 'ppo', 'acc', 'name', 'fma', 'monthly_date', 'remarks')
    else:
     messages.warning(request, "No employees found with remark: Cvp not issued")
    retired_pension_employees = Employee.objects.filter(remarks__icontains='Retired')
    if retired_pension_employees.exists():
     employee_by_remarks['Retired'] = retired_pension_employees.values('pk_ppo', 'ppo', 'acc', 'name', 'fma', 'monthly_date', 'remarks')
    else:
     messages.warning(request, "No employees found with remark: Retired")
    over_pension_employees = Employee.objects.filter(remarks__icontains='Over')
    if over_pension_employees.exists():
     employee_by_remarks['Over'] = over_pension_employees.values('pk_ppo', 'ppo', 'acc', 'name', 'fma', 'monthly_date', 'remarks')
    else:
     messages.warning(request, "No employees found with remark: Retired")
    new_pension_employees = Employee.objects.filter(remarks__icontains='New Family Pension')
    if new_pension_employees.exists():
     employee_by_remarks['New Family Pension'] = new_pension_employees.values('pk_ppo', 'ppo', 'acc', 'name', 'fma', 'monthly_date', 'remarks')
    else:
     messages.warning(request, "No employees found with remark: New Family Pension")
    attain_pension_employees = Employee.objects.filter(remarks__icontains='Attained 25')
    if attain_pension_employees.exists():
     employee_by_remarks['Attained 25'] = attain_pension_employees.values('pk_ppo', 'ppo', 'acc', 'name', 'fma', 'monthly_date', 'remarks')
    else:
     messages.warning(request, "No employees found with remark: Attained 25")
    return render(request, 'employee_table.html', {'employee_by_remarks': employee_by_remarks})



def generateDataNextMonth(request):
    if request.method == 'POST':
        genMonth: str = f"{request.POST['month']}"
        print(genMonth)
        drp: int = int(request.POST['drp'])
        basic_pay: int = int(request.POST['basic_pay'])
        print(basic_pay)
        perviousMonth = datetime.strptime(genMonth, '%Y-%m-%d') - timedelta(days=17)
        print(perviousMonth)
        perviousMonth = perviousMonth.strftime('%Y-%m-%d')[0:7]
        print(perviousMonth)
        employee = Employee.objects.filter(monthly_date=perviousMonth)
        for x in employee:
            bank = x.bank
            ppo = x.ppo
            monthly_date = genMonth[0:7]
            pk_ppo =f'{genMonth[0:7]}/{ppo}'
            acc = x.acc
            name = x.name
            age = x.age
            designation = x.designation
            types = x.types
            basic = basic_pay
            additional = x.additional
            total1 = x.total1
            comuted = x.comuted
            residuary = x.residuary
            dr = int(basic * (drp/100))
            fma = x.fma
            emolume = x.emolume
            amount = x.amount
            less = x.less
            Total2 = x.Total2
            remarks = x.remarks
            remarks_text = x.remarks_text
            remarks_date = datetime.today()
            issued_at = x.issued_at
            print(pk_ppo)
            Employee.objects.create(bank=bank, ppo=ppo, monthly_date=monthly_date, pk_ppo=pk_ppo, acc=acc, name=name, age=age, designation=designation, types=types, additional=additional, total1=total1, Total2=Total2, comuted=comuted, residuary=residuary, dr=dr, fma=fma, emolume=emolume, amount=amount, less=less, remarks=remarks, remarks_date=remarks_date, remarks_text=remarks_text, issued_at=issued_at)
        return render(request, template_name='genForm.html')
    else:
        return render(request, template_name='genForm.html')
