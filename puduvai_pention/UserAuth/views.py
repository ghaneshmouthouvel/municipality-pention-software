from django.shortcuts import render, redirect, resolve_url
from django.contrib.auth import authenticate, login
from django.contrib.auth import logout
from django.contrib.auth.models import User
from django.contrib import messages
from django.views.decorators.http import require_GET, require_POST


# Create your views here.


def signin(request):
    if request.user.is_authenticated:
        return redirect(resolve_url('dashboard'))
    else:
        if request.method == "POST":
            try:
                user_obj = User.objects.get(email=request.POST['email'])
            except User.DoesNotExist:
                user_obj = None
            if user_obj:
                password = request.POST['password']
                user = authenticate(request, username=user_obj.username, password=password)
                if user is not None:
                    login(request, user)
                    return redirect('dashboard')
                #  return render(request,'index.html')# Redirect to a success page.
            return render(request, 'demo.html', {'error': 'User Does Not Exist'})
        else:
            
            # Return an 'invalid login' error message.
            return render(request,'demo.html')
        
    #return render(request, 'templates/demo.html')
def logout_view(request):
    logout(request)
    return redirect('/')